import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AdminModule } from 'app/admin/admin.module';
import { CoreModule } from 'app/core/core.module';
import { ShoppingModule } from 'app/shopping/shopping.module';
import { SharedModule } from 'shared/shared.module';
//import {CarouselModule} from 'angular4-carousel';
import { environment } from './../environments/environment';
import { AppComponent } from './app.component';
import { ProductsComponent } from './shopping/components/products/products.component';
import { HomeComponent } from './home/home.component';
import { HomeModule } from './home/home.module';
//import { FooterComponent } from 'shared/components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
   // FooterComponentComponent
  ],
  imports: [
    //MDBBootstrapModule.forRoot(),
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    CoreModule,
    SharedModule,
    AdminModule,
    ShoppingModule,
    HomeModule,
    
    RouterModule.forRoot([
      { path: '', component: HomeComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
