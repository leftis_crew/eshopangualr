import { Observable } from 'rxjs/Observable';
import { ShoppingCart } from 'shared/models/shopping-cart';
import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { IAppUser } from 'shared/models/app-user';
import { AuthService } from 'shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {

  icon: boolean = true;
  navbarCollapsed: boolean;
  appUser: IAppUser = {} as IAppUser;
  totalCart$: Observable<ShoppingCart>;

  constructor(private cartService: ShoppingCartService, private authService: AuthService) { }

  async ngOnInit() {
    this.icon = true;
    this.authService.appUser$.subscribe(user => this.appUser = user);
    this.totalCart$ = await this.cartService.getCart();
  }

  logout() {
    this.authService.logout();
  }
  changeIcon(){
    this.icon = !this.icon;
    console.log(this.icon);
  }

}
