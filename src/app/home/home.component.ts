import { Component, OnInit } from '@angular/core';
import { ICarouselConfig, AnimationConfig } from 'angular4-carousel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public imageSources: string[] = [
    //TODO: retrieve them from db
    '../../assets/unsplash_images/oranges.jpg',
    '../../assets/unsplash_images/cookies.jpg',
    '../../assets/unsplash_images/milk.jpg'
 ];
 icon:boolean = true;
 public config: ICarouselConfig = {
   verifyBeforeLoad: true,
   log: false,
   animation: true,
   animationType: AnimationConfig.SLIDE,
   autoplay: true,
   autoplayDelay: 2000,
   stopAutoplayMinWidth: 1768
 };
  constructor() { }

  ngOnInit() {
  
  }
  changeIcon(){
    this.icon = !this.icon;
    console.log(this.icon);
  }

}
