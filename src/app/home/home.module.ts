import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarouselModule} from 'angular4-carousel';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { SwiperModule } from 'ngx-swiper-wrapper';
import {SwiperComponent} from "ngx-swiper-wrapper";
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

const SWIPER_CONFIG: SwiperConfigInterface = {
  scrollbar: null,
  effect:'slide',
  fade: {
    crossFade: true
    },
  cube: {
    slideShadows: true,
    shadow: true,
    shadowOffset: 20,
    shadowScale: 0.94
    },
  direction: 'horizontal',
  
  slidesPerView: 1,
  //spaceBetween:150,
  autoplay:1000,
  scrollbarHide: false,
  keyboardControl: true,
  mousewheelControl: true,
  scrollbarDraggable: true,
  scrollbarSnapOnRelease: true,
  pagination: '.swiper-pagination',
  paginationClickable: true,
  nextButton: '.swiper-button-next',
  prevButton: '.swiper-button-prev',
  //autoplayStopOnLast: true,
  slideToClickedSlide:true,
  loop:true,
  speed:3000,
  paginationHide:false
 
};

@NgModule({
  imports: [
    CommonModule,
    CarouselModule,
    SwiperModule.forRoot(SWIPER_CONFIG),
      RouterModule.forChild([{
      path: 'home',component:HomeComponent
    }])
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
